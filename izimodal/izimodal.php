<html>
<head>
<link rel="stylesheet" href="./plugins/izimodal/iziModal.css">
<script src='https://code.jquery.com/jquery-2.2.4.min.js'></script>
<script src="./plugins/izimodal/iziModal.js" type="text/javascript"></script>
</head>
<body>
<div id="modal-alert" data-izimodal-title="Parabéns" data-izimodal-subtitle="Edição efetuada com sucesso"></div>
    

</body>
</html>
<script>
$('#modal-alert').iziModal('open');

$('#modal-alert').iziModal({
    title: "Your message has been sent successfully",
    icon: 'icon-check',
    headerColor: '#00af66',
    width: 600,
    timeout: 5000,
    timeoutProgressbar: true,
    transitionIn: 'fadeInUp',
    transitionOut: 'fadeOutDown',
    bottom: 0,
    loop: true,
    autoOpen: 1,
    pauseOnHover: true
});
    
$(document).on('click', '.open-alert', function (event) {
  event.preventDefault();
  $('#modal-alert').iziModal('open');
});   
</script>